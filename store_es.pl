#!/usr/bin/perl
use strict;
use warnings;

# Grocery store inventory:
my $lines = <<'END_OF_REPORT';
0.95  300   Peaches
1.45  120   Avocados
5.50   10   Durien
0.40  700   Apples
END_OF_REPORT

my ($line1, $line2, $line3, $line4) = split "\n", $lines;
my ($cost, $quantity, $item) = split " ", $line1;  
if (index($item, 'es') != -1)
{
	print "Total value of $item on hand = \$", $cost * $quantity, "\n";
}
($cost, $quantity, $item) = split " ", $line2;  
if (index($item, 'es') != -1)
{
	print "Total value of $item on hand = \$", $cost * $quantity, "\n";
}
($cost, $quantity, $item) = split " ", $line3;  
if (index($item, 'es')!= -1)
{
	print "Total value of $item on hand = \$", $cost * $quantity, "\n";
}
($cost, $quantity, $item) = split " ", $line4;  
if (index($item, 'es') != -1)
{
	print "Total value of $item on hand = \$", $cost * $quantity, "\n";
}
